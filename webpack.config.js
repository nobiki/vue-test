var ExtractTextPlugin = require("extract-text-webpack-plugin");
var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: {
        "common": path.join(__dirname, "./webpack/common/entry.js"),
        "app/top": path.join(__dirname, "./webpack/app/top/entry.js"),
    },
    output: {
        filename: "./public/assets/[name]/bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract({fallback:'style-loader',use:'css-loader'})
            },
            {
                test: /\.(jpg|gif|png|woff|woff2|eot|ttf|svg)$/,
                loader: 'url-loader?limit=100000'
            },
        ]
    },
    resolve: {
        alias: {
            'vue': 'vue/dist/vue.esm.js',
            'vuex': 'vuex/dist/vuex.esm.js',
            'vue-router': 'vue-router/dist/vue-router.esm.js'
        }
    },
    plugins: [
        new ExtractTextPlugin("./public/assets/[name]/bundle.css")
    ]
};
