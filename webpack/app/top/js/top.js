import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import routes from '../router/routes.js';

Vue.use(Vuex);
Vue.use(VueRouter);

const router = new VueRouter({
  routes
});

new Vue({
  el: "*[data-route='contents']",
  router: router,
});

new Vue({
  el: "*[data-route='topNav']",
  router: router,
});

new Vue({
  el: "*[data-route='footNav']",
  router: router,
});
