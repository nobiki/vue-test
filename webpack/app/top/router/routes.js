import indexView from './../view/indexView.vue';
import linkAView from './../view/linkAView.vue';
import linkBView from './../view/linkBView.vue';
import linkCView from './../view/linkCView.vue';

export default [
  { path: '/', component: indexView },
  { path: '/linkA', component: linkAView },
  { path: '/linkB', component: linkBView },
  { path: '/linkC', component: linkCView },
];
